package Tests;

import org.junit.jupiter.api.Test;
import produkty.ControlOfOrder;
import produkty.Pizza;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static produkty.ControlOfOrder.getOrder;

public class ControlOfOrderTest {

    static Pizza pizza;
    static ControlOfOrder controlOfOrder = new ControlOfOrder();

    @Test
    void createPizzaTest(){
        controlOfOrder.createPizza("Testowa Pizza");
        assertTrue(controlOfOrder.getPizza().getName()=="Testowa Pizza");
    }

    @Test
    void addExtraIngridientsTest(){
        controlOfOrder.createPizza("Testowa Pizza");
        controlOfOrder.addExtraIngredients(true,true,true,true,true,true,true);
        assertTrue(controlOfOrder.getPizza().getMushrooms()==2);
        assertTrue(controlOfOrder.getPizza().getCheese()==2);
        assertTrue(controlOfOrder.getPizza().getPineapple()==2);
        assertTrue(controlOfOrder.getPizza().getBacon()==2);
        assertTrue(controlOfOrder.getPizza().getCorn()==2);
        assertTrue(controlOfOrder.getPizza().getTomatoes()==2);

    }

    @Test
    void addPizzaToOrderTest(){
        controlOfOrder.createPizza("Testowa Pizza");
        controlOfOrder.addPizzaToOrder();
        assertTrue(getOrder().getProduct(0) instanceof Pizza);
    }

}
