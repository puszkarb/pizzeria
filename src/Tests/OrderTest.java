package Tests;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import produkty.Drink;
import produkty.Order;
import produkty.Pizza;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class OrderTest {

    static Order order= new Order( 0,"z1", "test", "nowe");
    static Pizza pizza = new Pizza("Elektryzujaca", 150 );
    static Drink water = new Drink( "woda" );


    @BeforeAll
    static void setUp(){
        order.addProduct(pizza);
        order.addProduct(water);
    }

    @Test
    void testGetProduct() {
        assertEquals(pizza, order.getProduct(0));
        assertEquals(water, order.getProduct(1));
    }

    @Test
    void testHowManyProducts() {
        assertEquals(2,order.howManyProducts());
    }

    @Test
    void pizzaToDBStringTest() {
        String s1 = order.pizzaToDBString();
        String s2 = "Elektryzujaca ser: 2 pomidory: 1 pieczarki: 0 szynka: 1 ananas: 1 kukurydza: 1 bekon: 0 ciasto: 0 rozmiar: 150 ";
        assertEquals(s1, s2);
    }

    @Test
    void drinkToDBStringTest() {
        String s1 = order.drinkToDBString();
        String s2 = "woda pojemnosc: 1 lod: true ";
        assertEquals(s1, s2);
    }


}
