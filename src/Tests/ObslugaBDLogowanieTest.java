package Tests;

import ObslugaBD.DBConnectionLogin;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ObslugaBDLogowanieTest {

    @Test
    void sprawdzCzyJestWBazie1() {
        assertFalse(DBConnectionLogin.chechIfIsInDatabase("Email ktorego nie w bd" ,"qwer"));
    }

    @Test
    void sprawdzCzyJestWBazie2() {
        assertFalse(DBConnectionLogin.chechIfIsInDatabase("wlodarczyk_pawel1@wp.pl" ,"zle haslo"));
    }

    @Test
    void sprawdzCzyJestWBazie3() {
        assertTrue(DBConnectionLogin.chechIfIsInDatabase("mail@pl.pl" ,"qwerty"));
    }
}