package Tests;

import org.junit.jupiter.api.Test;
import produkty.Drink;

import static org.junit.jupiter.api.Assertions.*;

public class DrinkTest {
    static Drink sevenUp = new Drink("Sevenupduza");
    static Drink pepsi = new Drink("Pepsimala");

    @Test
    void checkNameTest() {
        assertEquals("Sevenupduza", sevenUp.getName());
        assertEquals("Pepsimala", pepsi.getName());
    }

    @Test
    void addLodTest() {
        sevenUp.setIce(true);
        assertTrue(sevenUp.getIce());
        sevenUp.setIce(false);
        assertFalse(sevenUp.getIce());
    }

    @Test
    void checkSizeTest() {
        assertEquals(1, sevenUp.getSize());
        assertEquals(500, pepsi.getSize());
        assertNotEquals(1, pepsi.getSize());

        pepsi.setSize(6969);
        assertEquals(6969, pepsi.getSize());
    }
}
