package produkty;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import server_client.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Order implements Serializable{
    private int order_id;
    private List<Product> products;
    private String name;
    private int price;
    private int pkt;
    private String comment;
    private String status;
    private User client;

    private IntegerProperty order_id_Property;

    public int getOrder_id_Property() {
        return order_id_Property.get();
    }

    public IntegerProperty order_id_PropertyProperty() {
        return order_id_Property;
    }

    private StringProperty nameProperty;

    public String getNameProperty() {
        return nameProperty.get();
    }

    public StringProperty namePropertyProperty() {
        return nameProperty;
    }

    public String getCommentProperty() {
        return commentProperty.get();
    }

    public StringProperty commentPropertyProperty() {
        return commentProperty;
    }

    public String getStatusProperty() {
        return statusProperty.get();
    }

    public StringProperty statusPropertyProperty() {
        return statusProperty;
    }

    private StringProperty commentProperty;
    private StringProperty statusProperty;


    public Order(int id, String name, String comment, String status )
    {
        this.order_id = id;
        this.name = name;
        this.comment = comment;
        this.status = status;
        this.client = null;
        this.products = new ArrayList<>();
        this.price = 0;
        this.pkt = 0;

        this.order_id_Property = new SimpleIntegerProperty(id);
        this.nameProperty = new SimpleStringProperty(name);
        this.commentProperty = new SimpleStringProperty(comment);
        this.statusProperty = new SimpleStringProperty(status);

    }

    public String pizzaToDBString(){
        StringBuilder pizza = new StringBuilder();
        for (int i = 0; i< products.size(); i++){
            if(getProduct(i) instanceof Pizza)
                pizza.append(getProduct(i).toString());
        }
        return pizza.toString();
    }
    public String drinkToDBString(){
        StringBuilder drink= new StringBuilder();
        for(int i = 0; i< products.size(); i++){
            if (getProduct(i) instanceof Drink)
                drink.append(getProduct(i).toString());
        }
        return drink.toString();
    }


    public int getPrice (){
        return price;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    public void changeStatus ( String newStatus )
    {
        this.status = newStatus;
    }

    public void addProduct ( Product p1 )
    {
        products.add( p1 );
    }

    public void removeProduct ( Product p1 )
    {
        products.remove( p1 );
    }

    public void removeProduct ( int id )
    {
        products.remove( id );
    }

    public int howManyProducts ()
    {
        return products.size();
    }

    public Product getProduct (int id )
    {
        return products.get( id );
    }

    public void setPkt(int pkt)
    {
        this.pkt = pkt;
    }

    public int getPkt()
    {
        return pkt;
    }

    public void setOrder_id( int order_id){
        this.order_id= order_id;
    }
    public int getId() {
        return order_id;
    }

    public void setClient(User client) {
        this.client = client;
    }

    public User getClient() {
            return client;
    }

    public String getComment()
    {
        return comment;
    }

    public String getStatus(){
        return status;
    }



    @Override
    public String toString(){
        return "Order: \n\tID:  " + order_id + " \n\tPunkty: " + pkt +  "\n\tKomentarz: " + comment + "\n\tStatus: " + status;

    }

}
