package produkty;

public class Drink extends Product {

    private int size;
    private Boolean ice;

    public Drink(String name )
    {

        super( name, 5, 1);
        switch (name) {
            case "Pepsimala":
                size = 500;
                ice =true;
                break;

            case "Pepsiduza":
                size = 1;
                ice = true;
                break;


            case "Sevenupmala":
                size = 500;
                ice = true;
                break;

            case "Sevenupduza":
                size = 1;
                ice = true;
                break;

            default:
                size = 1;
                ice = true;
                break;
        }

    }





    /*public Napoj( String nazwa, double cena, int pkt )
    {
        super( nazwa, cena, pkt);
    }*/

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public Boolean getIce() {
        return ice;
    }

    public void setIce(Boolean ice) {
        this.ice = ice;
    }

    @Override
    public String toString()
    {
        StringBuilder sklad = new StringBuilder();
        sklad.append(getName()).append(" ");
        sklad.append("pojemnosc: ").append(size).append(" ");
        sklad.append("lod: ").append(ice).append(" ");

        return  sklad.toString();
    }

}
