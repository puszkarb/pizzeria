package produkty;

public class Pizza extends Product {

    private int cheese;
    private int tomatoes;
    private int mushrooms;
    private int ham;
    private int pineapple;
    private int corn;
    private int bacon;
    private int dough;
    private int size;

    public Pizza( String name, int size )
    {

        super( name, 5, 1);
        this.size = size;
        switch (name) {
            case "Studencka":
                cheese = 1;
                tomatoes = 1;
                mushrooms = 0;
                ham = 0;
                pineapple = 0;
                corn = 0;
                bacon = 0;
                break;

            case "Wezuwiusz":
                cheese = 2;
                tomatoes = 1;
                mushrooms = 0;
                ham = 1;
                pineapple = 0;
                corn = 0;
                bacon = 0;
                break;


            case "Nowowiejska":
                cheese = 1;
                tomatoes = 1;
                mushrooms = 0;
                ham = 0;
                pineapple = 0;
                corn = 0;
                bacon = 1;
                break;

            case "Elektryzujaca":
                cheese = 2;
                tomatoes = 1;
                mushrooms = 0;
                ham = 1;
                pineapple = 1;
                corn = 1;
                bacon = 0;
                break;

            default:
                cheese = 1;
                tomatoes = 1;
                mushrooms = 1;
                ham = 1;
                pineapple = 1;
                corn = 1;
                bacon = 1;
                break;
        }

    }


    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getTomatoes() {
        return tomatoes;
    }

    public void setTomatoes(int tomatoes) {
        this.tomatoes = tomatoes;
    }

    public int getMushrooms() {
        return mushrooms;
    }

    public void setMushrooms(int mushrooms) {
        this.mushrooms = mushrooms;
    }

    public int getCheese() {
        return cheese;
    }

    public void setCheese(int cheese) {
        this.cheese = cheese;
    }

    public int getHam() {
        return ham;
    }

    public void setHam(int ham) {
        this.ham = ham;
    }

    public int getPineapple() {
        return pineapple;
    }

    public void setPineapple(int pineapple) {
        this.pineapple = pineapple;
    }

    public int getCorn() {
        return corn;
    }

    public void setCorn(int corn) {
        this.corn = corn;
    }

    public int getBacon() {
        return bacon;
    }

    public void setBacon(int bacon) {
        this.bacon = bacon;
    }

    public int getDough() {
        return dough;
    }

    public void setDough(int dough) {
        this.dough = dough;
    }

    @Override
    public String toString()
    {
        StringBuilder sklad = new StringBuilder();
        sklad.append(getName()).append(" ");
        sklad.append("ser: ").append(cheese).append(" ");
        sklad.append("pomidory: ").append(tomatoes).append(" ");
        sklad.append("pieczarki: ").append(mushrooms).append(" ");
        sklad.append("szynka: ").append(ham).append(" ");
        sklad.append("ananas: ").append(pineapple).append(" ");
        sklad.append("kukurydza: ").append(corn).append(" ");
        sklad.append("bekon: ").append(bacon).append(" ");
        sklad.append("ciasto: ").append(dough).append(" ");
        sklad.append("rozmiar: ").append(size).append(" ");
        return  sklad.toString();
    }
}
