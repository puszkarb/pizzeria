package ObslugaBD;

import server_client.User;

import java.sql.*;
/**
 * Created by Paweł on 04.04.2018.
 */
public class DBConnectionLogin extends DBConnection {

    public static boolean addUser(String email, String password, String address, String tel) {
        boolean success = false;
        try {
            Connection conn = connectToDatabase();
            Statement sqlStat = conn.createStatement();

            String query = "insert into pizzeria.dane_klientow(email, haslo, ulica, telefon, salt) values(?,?,?,?,?)";
            String salt = PasswdEncrypt.getSalt(40);
            PreparedStatement prSt = conn.prepareStatement(query);
            prSt.setString(1, email);
            prSt.setString(2, PasswdEncrypt.generateSecurePassword(password, salt));
            prSt.setString(3, address);
            prSt.setString(4, tel);
            prSt.setString(5, salt);
            prSt.execute();
            System.out.println("Dodano uzytkownika");
            success = true;
            conn.close();


        }catch (SQLException ex){
            System.err.println("Error connecting to the database1");
            ex.printStackTrace(System.err);
            System.exit(0);
        }
        return success;

    }

   public static boolean chechIfIsInDatabase(String email, String haslo){
        boolean isInDatabase = false;
        try {
            Connection conn = connectToDatabase();
            Statement sqlStat = conn.createStatement();
            ResultSet rs = sqlStat.executeQuery("select p.email, p.haslo, p.salt from pizzeria.dane_klientow p where p.email = '" + email + "'");
            if (rs.next()){
                String emailZBazy = rs.getString("email");
                String hasloZBazy = rs.getString("haslo");
                String saltZBazy = rs.getString("salt");
                if(emailZBazy.equals(email) && PasswdEncrypt.verifyUserPassword(haslo,hasloZBazy,saltZBazy)){
                    isInDatabase = true;
                    System.out.println("Poprawna weryfikacja - osoba jest w bazie");
                    return isInDatabase;
                }
            }
            conn.close();
        }catch (SQLException ex){
            System.err.println("Error connecting to the database");
            ex.printStackTrace(System.err);
            System.exit(0);
        }
        System.out.println("Tej osoby nie ma w bazie");
        return isInDatabase;

    }

    public static User getUserFromDatabase( String email ){
        User returnedUser = new User();
        return returnedUser;
    }
}
