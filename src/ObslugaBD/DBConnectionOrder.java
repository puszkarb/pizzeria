package ObslugaBD;

import produkty.Order;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Paweł on 04.04.2018.
 */
public class DBConnectionOrder extends DBConnection {

    public static void addOrderToDatabase(Order order){
        try {
            Connection conn = connectToDatabase();
            Statement sqlStat = conn.createStatement();
            String query = "insert into pizzeria.zamowienia(pizza_sklad,sosy,napoje,ulica,telefon"
                    + ",status, komentarz,iloscPkt,cena) values('" + order.pizzaToDBString()+ "','" + "SOS" + "','" + order.drinkToDBString()
                    + "','" + "ADRES" + "','" + "TEL" + "','" + order.getStatus() + "','"
                    + order.getComment() + "','" + order.getPkt() + "','" + order.getPrice()  + "')";
            System.out.println(query);
            sqlStat.executeUpdate(query);
            ResultSet rs = sqlStat.executeQuery("select max(p.id_zamowienia) from pizzeria.zamowienia p");
            rs.next();
            order.setOrder_id(rs.getInt(1));
            System.out.println("Dodano zamowienie o numerze " + order.getId());
            conn.close();
        }
        catch (SQLException ex){
            System.err.println("Error connecting to the database3");
            ex.printStackTrace(System.err);
            System.exit(0);
        }

    }

    public static void changeOrderState(Order order, String newState){
        try {
            Connection conn = connectToDatabase();
            Statement sqlStat = conn.createStatement();
            String query = "update pizzeria.zamowienia set status='" + newState +"' where id_zamowienia = " + order.getId();
            sqlStat.executeUpdate(query);
            order.changeStatus(newState);
            System.out.println("Zmieniono status zamowienia o id " + order.getId() + " na " + order.getStatus());
            conn.close();
        }catch (SQLException ex){
            System.err.println("Error connecting to the database3");
            ex.printStackTrace();
            System.exit(0);
        }
    }

}
