package server_client;

import ObslugaBD.DBConnectionLogin;

import java.io.*;
import java.net.Socket;

import static ObslugaBD.DBConnectionLogin.chechIfIsInDatabase;


public class ServerWorker extends Thread{

    private final Socket clientSocket;

    private final Server server;
    private String login = null;
    private ObjectOutputStream objectOutputStream;



    public ServerWorker(Server server, Socket clientSocket) {
        this.server = server;
        this.clientSocket = clientSocket;

    }

    @Override
    public void run() {
        try {
            this.handleClientConnection();


        } catch ( IOException | InterruptedException | ClassNotFoundException e) {

        }

    }
    private void handleClientConnection() throws IOException, ClassNotFoundException, InterruptedException {

        InputStream inputStream = this.clientSocket.getInputStream();
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);

        System.out.println("Przed utworzyc obiekt output");

       OutputStream outputStream = this.clientSocket.getOutputStream();
       this.objectOutputStream = new ObjectOutputStream(outputStream);
       System.out.println("Udało się utworzyc obiekt output");


        Object incomingObject;


        while(true) {

            incomingObject = objectInputStream.readObject();
            if (incomingObject instanceof Zamowienie) {
                System.out.println("zamowineie");
                this.handleNewOrder(incomingObject);

            } else if( incomingObject instanceof User && ((User) incomingObject).getUlica() == null){
                this.handLogin(incomingObject);
                System.out.println("Udało się handLogin");


            }else if (incomingObject instanceof User) {
                System.out.println("user");
                this.handleNewUser(incomingObject);

            }
        }



    }

    private void handleNewOrder(Object incomingObject) {
        Zamowienie noweZamowienie = (Zamowienie) incomingObject;
        System.out.println(noweZamowienie.toString());
    }

    private void handleNewUser(Object incomingObject) {
        System.out.println(incomingObject.toString());
        User newUser = (User) incomingObject;

        if(!chechIfIsInDatabase(newUser.getEmail(), newUser.getHaslo())) {
            DBConnectionLogin.addUser(newUser.getEmail(), newUser.getHaslo(), newUser.getUlica(), newUser.getTel());
            try {
                objectOutputStream.writeObject(true);
            } catch (IOException e) {
                System.out.println("Niepowodzenie w komunikacji zwrotnej");
            }
        }
        else {
            try {
                objectOutputStream.writeObject(false);
            } catch (IOException e) {
                System.out.println("Niepowodzenie w komunikacji zwrotnej w elsie");
            }

        }
    }
    private void handLogin(Object incomingObject){
        User existingUser= (User) incomingObject;
        if(chechIfIsInDatabase(existingUser.getEmail(),existingUser.getHaslo())){

            try{
                objectOutputStream.writeObject(true);
            }catch (IOException e){

            }
        }
        else{
            try{
                objectOutputStream.writeObject(false);
            }catch (IOException e){

            }
        }
    }
}
